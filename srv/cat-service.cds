using ZCAP001 as z from '../db/data-model';

service VIEW_ZCAP001 {
    entity ZTHDB001_SRV as projection on z.ZTHDB001;
    entity ZTHDB002_SRV as projection on z.ZTHDB002;
    entity ZTHDB003_SRV as projection on z.ZTHDB003;
}
