namespace ZCAP001;

// CCS-小分類
entity ZTHDB001 {
    key S_ID           : String(2) not null;
        DESCRIPTION_ZF : String(100);
        DESCRIPTION_EN : String(100);
        ACTIVE         : String(1);

}

// CCS-中分類
entity ZTHDB002 {
    key M_ID           : String(2) not null;
        L_ID           : String(2);
        DESCRIPTION_ZF : String(100);
        DESCRIPTION_EN : String(100);
        FORMAT         : String(30);
        ACTIVE         : String(1);

}

// CCS-大分類
entity ZTHDB003 {
    key L_ID           : String(2) not null;
        DESCRIPTION_ZF : String(100);
        DESCRIPTION_EN : String(100);
        SEQ            : String(10);
        ACTIVE         : String(1);

}
